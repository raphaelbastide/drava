let d = document
let b = d.body
let main = b.querySelector('main')
let notesInput = b.querySelector('.notes')
let drawing = []
let penUp = false
const spacing = 15
const cols = 50 // must be even
const dotSize = 2
const dotNbr = cols * cols
const size = (cols - 1) * spacing + dotSize

main.style.height = size+"px"
main.style.width = size+"px"

document.addEventListener("keypress", function(event) {
  const key = event.key;
  if (key >= 1 && key <= 9) {
    addLine(key);
  }
});

function addLine(key){
  if (key == 8){sendNote("C")}
  if (key == 9){sendNote("C#")}
  if (key == 6){sendNote("D")}
  if (key == 3){sendNote("D#")}
  if (key == 2){sendNote("E")}
  if (key == 1){sendNote("F")}
  if (key == 4){sendNote("F#")}
  if (key == 7){sendNote("G")}
  if (key == 5){sendNote("G#")}
}

function sendNote(note){
  addNote(note)
  if (note == "C"){dclass = ".north"}
  else if (note == "C#"){dclass = ".north-east"}
  else if (note == "D"){dclass = ".east"}
  else if (note == "D#"){dclass = ".south-east"}
  else if (note == "E"){dclass = ".south"}
  else if (note == "F"){dclass = ".south-west"}
  else if (note == "F#"){dclass = ".west"}
  else if (note == "G"){dclass = ".north-west"}
  else if (note == "G#"){dclass = ".penUp", penUp = true}
  else if (note == "A"){dclass = ".unused"}
  else if (note == "A#"){dclass = ".point"}
  else if (note == "B"){dclass = ".penUp", penUp = false}
  else{dclass = ".first"}
  let origin = d.querySelector('.origin')
  let destination = d.querySelector(dclass)
  if (destination == undefined) {
    destination = d.querySelector(".origin")
  }
  abconnect(origin, destination, penUp)
  select(destination)
}

function addNote(note){
  notesInput.value += note+","
}

// Create the dots
for (let i = 0; i < dotNbr; i++) {
  const dot = document.createElement('div');
  dot.classList.add('dot');
  if (i == dotNbr / 2 + 25 ) {
    dot.classList.add('origin','first')
  }
  main.appendChild(dot);
}

const dots = main.querySelectorAll('.dot');
select(d.querySelector('.origin'))
for (let i = 0; i < dots.length; i++) {
  const row = Math.floor(i / cols);
  const col = i % cols;
  const topPos  = row * spacing;
  const leftPos = col * spacing;
  dots[i].style.top  = topPos  + 'px';
  dots[i].style.left = leftPos + 'px';
}

// Add click event listener
function select(dot) {
  const selected = Array.from(dots).indexOf(dot);
  const surroundingIndices = getSurroundingIndices(selected);
  for (const dot of dots) {
    dot.classList.remove('north', 'north-east', 'east', 'south-east', 'south', 'south-west', 'west', 'north-west','origin');
  }
  for (const index of surroundingIndices) {
    const dot = dots[index];
    const direction = getDirection(selected, index);
    dot.classList.add(direction);
  }
  dot.classList.add('origin')
}

function getSurroundingIndices(clickedIndex) {
  const row = Math.floor(clickedIndex / cols);
  const col = clickedIndex % cols;
  const surroundingIndices = [];
  for (let i = row - 1; i <= row + 1; i++) {
    for (let j = col - 1; j <= col + 1; j++) {
      if (i >= 0 && i < cols && j >= 0 && j < cols && !(i === row && j === col)) {
        surroundingIndices.push(i * cols + j);
      }
    }
  }
  return surroundingIndices;
}

function getDirection(clickedIndex, targetIndex) {
  const clickedRow = Math.floor(clickedIndex / cols);
  const clickedCol = clickedIndex % cols;
  const targetRow = Math.floor(targetIndex / cols);
  const targetCol = targetIndex % cols;
  if (targetRow < clickedRow) {
    if (targetCol < clickedCol) {
      return 'north-west';
    } else if (targetCol === clickedCol) {
      return 'north';
    } else {
      return 'north-east';
    }
  } else if (targetRow === clickedRow) {
    if (targetCol < clickedCol) {
      return 'west';
    } else {
      return 'east';
    }
  } else {
    if (targetCol < clickedCol) {
      return 'south-west';
    } else if (targetCol === clickedCol) {
      return 'south';
    } else {
      return 'south-east';
    }
  }
}

d.querySelector('.play').onclick = function(){
  playLine()
}
function playLine(){
  let sheet = notesInput.value
  sheet = sheet.split(',')
  console.log(sheet);
  let note = new Note()
  let i = 0
  var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
  var oscillator = audioCtx.createOscillator();
  var gainNode = audioCtx.createGain();
  oscillator.connect(gainNode); 
  gainNode.connect(audioCtx.destination);
  oscillator.type = 'sine'; 
  gainNode.gain.value = .5;
  oscillator.start()
  setInterval(() => {
    console.log(sheet[i]);
    freq = note.getFreqFromNote(sheet[i], 'just')
    oscillator.frequency.value = freq +200;
    i++
  }, 100);
}